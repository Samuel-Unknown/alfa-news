package com.samuelunknown.alfa_news;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.samuelunknown.alfa_news.about.AboutActivity;
import com.samuelunknown.alfa_news.news.DbNewsItem;
import com.samuelunknown.alfa_news.news.NewsContract;
import com.samuelunknown.alfa_news.news.NewsFragment;
import com.samuelunknown.alfa_news.utilities.StatusBarUtils;
import com.samuelunknown.alfa_news.utilities.ToolBarUtils;
import com.samuelunknown.alfa_news.web.WebActivity;

import java.util.ArrayList;
import java.util.List;

//Баги:
// region список
// endregion

//Сделать:
//region список
//endregion

public class MainActivity extends AppCompatActivity
        implements NewsContract.FragmentListener, NavigationView.OnNavigationItemSelectedListener {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String NAV_MENU_SECTION_ALL_NEWS = "AllNews";
    private static final String NAV_MENU_SECTION_FAVORITE_NEWS = "FavoriteNews";
    private static final String CURRENT_NAV_MENU_SECTION_KEY = "CurrentNavMenuSectionKey";
    //endregion

    //region Variables
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Runnable mPendingRunnableForNavView;
    private String mCurrentNavMenuSection;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private NewsContract.FragmentView mNewsFragmentView;

    private NewsIsFavoriteStateReceiver mReceiver;
    private IntentFilter mReceiveIntentFilter;
    //endregion

    //region Activity Main Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setCheckedItem(R.id.nav_news);
        //endregion

        //region Toolbar
        mToolbar = ToolBarUtils.setToolbarAsActionBar(this, R.id.main_activity_toolbar);
        //endregion

        //region StatusBar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion

        //region DrawerLayout & ActionBarDrawerToggle
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        final Handler handler = new Handler();

        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (mPendingRunnableForNavView != null) {
                    handler.post(mPendingRunnableForNavView);
                    mPendingRunnableForNavView = null;
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.setDrawerSlideAnimationEnabled(false);
        mActionBarDrawerToggle.syncState();
        //endregion

        //region Check savedInstanceState
        if (savedInstanceState != null) {
            mCurrentNavMenuSection = savedInstanceState.getString(CURRENT_NAV_MENU_SECTION_KEY);
        } else {
            mCurrentNavMenuSection = NAV_MENU_SECTION_ALL_NEWS;
        }
        //endregion

        //region OnBackStackChangedListener
        // Что бы при добавлении фрагмента в стек, менялась иконка "гамбургер" на иконку "назад"
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        actionBar.setDisplayHomeAsUpEnabled(true); // show back button
                        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onBackPressed();
                            }
                        });
                    } else {
                        //show hamburger
                        actionBar.setDisplayHomeAsUpEnabled(false);
                        mActionBarDrawerToggle.syncState();
                        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDrawerLayout.openDrawer(GravityCompat.START);
                            }
                        });
                    }
                }
            }
        });
        //endregion

        //region Антибаг исправление бага с названием в тулбаре,
        // установка напрямую не работает в onCreate()
        // решение взято тут -> https://stackoverflow.com/questions/26486730/in-android-app-toolbar-settitle-method-has-no-effect-application-name-is-shown
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (NAV_MENU_SECTION_ALL_NEWS.equals(mCurrentNavMenuSection)) {
                actionBar.setTitle(getResources().getText(R.string.title_activity_main_news_section));
            } else if (NAV_MENU_SECTION_FAVORITE_NEWS.equals(mCurrentNavMenuSection)) {
                actionBar.setTitle(getResources().getText(R.string.title_activity_main_favorites_section));
            }
        }
        //endregion

        //region Open Current Fragment
        if (NAV_MENU_SECTION_ALL_NEWS.equals(mCurrentNavMenuSection)) {
            mNavigationView.setCheckedItem(R.id.nav_news);
            showAllNewsFragment();
        } else if (NAV_MENU_SECTION_FAVORITE_NEWS.equals(mCurrentNavMenuSection)) {
            mNavigationView.setCheckedItem(R.id.nav_favorites);
            showFavoriteNewsFragment();
        }
        //endregion

        mReceiver = new NewsIsFavoriteStateReceiver();

        mReceiveIntentFilter = new IntentFilter();
        mReceiveIntentFilter.addAction(WebActivity.NEWS_MARKED_AS_FAVORITE_ACTION);
        mReceiveIntentFilter.addAction(WebActivity.NEWS_MARKED_AS_NOT_FAVORITE_ACTION);

        registerReceiver(mReceiver, mReceiveIntentFilter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(CURRENT_NAV_MENU_SECTION_KEY, mCurrentNavMenuSection);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }
    //endregion

    //region Implementation: NavigationView.OnNavigationItemSelectedListener
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_news) {
            //region showAllNewsFragment
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    showAllNewsFragment();
                }
            };
            //endregion
        } else if (id == R.id.nav_favorites) {
            //region openTrashFragment
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    showFavoriteNewsFragment();
                }
            };
            //endregion
        } else if (id == R.id.nav_about_app) {
            //region startSettingsActivity
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    startAboutAppActivity();
                }
            };
            //endregion
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
    //endregion

    //region Implementation: NewsContract.FragmentListener
    @Override
    public void onOpenNews(DbNewsItem selectedDbNewsItem) {
        startWebViewActivity(selectedDbNewsItem);
    }
    //endregion

    //region Show Fragments
    public void showAllNewsFragment() {
        replaceNewsFragment(NAV_MENU_SECTION_ALL_NEWS);
        showAllNewsSectionToolbar();
    }

    public void showFavoriteNewsFragment() {
        replaceNewsFragment(NAV_MENU_SECTION_FAVORITE_NEWS);
        showFavoriteNewsSectionToolbar();
    }

    private void replaceNewsFragment(@NonNull String menuSection) throws IllegalArgumentException {

        //region Argument Check
        if (!NAV_MENU_SECTION_ALL_NEWS.equals(menuSection) &&
                !NAV_MENU_SECTION_FAVORITE_NEWS.equals(menuSection)) {
            throw new IllegalArgumentException("Unknown menuSection: " + menuSection);
        }
        //endregion

        FragmentManager fragmentManager = getSupportFragmentManager();
        NewsFragment newsFragment =
                (NewsFragment) fragmentManager.findFragmentById(R.id.fragments_container);

        if (newsFragment == null || !menuSection.equals(mCurrentNavMenuSection)) {
            newsFragment =
                    NewsFragment.newInstance(NAV_MENU_SECTION_FAVORITE_NEWS.equals(menuSection));

            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragments_container, newsFragment)
                    .commit();
        }

        mCurrentNavMenuSection = menuSection;
        mNewsFragmentView = newsFragment;
    }

    private void showAllNewsSectionToolbar() {
        mToolbar.setTitle(getResources().getText(R.string.title_activity_main_news_section));
    }

    private void showFavoriteNewsSectionToolbar() {
        mToolbar.setTitle(getResources().getText(R.string.title_activity_main_favorites_section));
    }
    //endregion

    //region Start Activities
    private void startWebViewActivity(DbNewsItem dbNewsItem) {
        List<DbNewsItem> allDbNewsItems = mNewsFragmentView.getAllDbNewsItems();
        List<String> allDbNewsItemsInJsonFormat = new ArrayList<>();

        for (DbNewsItem item : allDbNewsItems) {
            allDbNewsItemsInJsonFormat.add(DbNewsItem.toJson(item));
        }

        Intent intent = new Intent(this, WebActivity.class);
        intent.putExtra(WebActivity.INTENT_SELECTED_DB_NEWS_ITEM_KEY, DbNewsItem.toJson(dbNewsItem));
        intent.putStringArrayListExtra(
                WebActivity.INTENT_ALL_DB_NEWS_ITEMS_KEY,
                (ArrayList<String>) allDbNewsItemsInJsonFormat);

        startActivity(intent);
    }

    private void startAboutAppActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    //endregion

    /**
     * BroadcastReceiver для получения информации о том, была ли в раздел Избранное
     * добавлена новость или что она была удалена от туда.
     */
    private class NewsIsFavoriteStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // Если новость была добавлена в раздел Избранное или наоборот удалена оттуда
            boolean rightAction = action.equals(WebActivity.NEWS_MARKED_AS_FAVORITE_ACTION) ||
                    action.equals(WebActivity.NEWS_MARKED_AS_NOT_FAVORITE_ACTION);

            boolean hasNewsItem = intent.hasExtra(WebActivity.NEWS_MARKED_ITEM_EXTENDED_DATA_KEY);
            boolean hasNewsIndex = intent.hasExtra(WebActivity.NEWS_MARKED_ITEM_INDEX_EXTENDED_DATA_KEY);

            if (!rightAction || !hasNewsItem || !hasNewsIndex) {
                return;
            }

            String jsonItem = intent.getStringExtra(WebActivity.NEWS_MARKED_ITEM_EXTENDED_DATA_KEY);
            DbNewsItem item = DbNewsItem.fromJson(jsonItem);

            int index = intent.getIntExtra(WebActivity.NEWS_MARKED_ITEM_INDEX_EXTENDED_DATA_KEY, -1);

            if (index != -1) {
                // Оповещаем об изменении этого элемента в списке новостей,
                // что бы в Recycler View были свежие данные
                // (цвет иконки меняется например и это должно быть сразу видно)
                mNewsFragmentView.swapItem(index, item);
            }
        }
    }
}