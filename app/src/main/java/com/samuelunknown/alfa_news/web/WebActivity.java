package com.samuelunknown.alfa_news.web;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.samuelunknown.alfa_news.R;
import com.samuelunknown.alfa_news.news.DbNewsItem;
import com.samuelunknown.alfa_news.utilities.CommunicationUtils;
import com.samuelunknown.alfa_news.utilities.StatusBarUtils;
import com.samuelunknown.alfa_news.utilities.ToolBarUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class WebActivity extends AppCompatActivity implements WebContract.FragmentListener {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = WebActivity.class.getSimpleName();

    /**
     *  ключ для вытаскивания DbNewsItem из Intent'а
     */
    public static final String INTENT_SELECTED_DB_NEWS_ITEM_KEY = "SelectedDbNewsItemKey";

    /**
     * Ключ для вытаскивания всех достпуных элементов DbNewsItem из Intent'а
     */
    public static final String INTENT_ALL_DB_NEWS_ITEMS_KEY = "AllDbNewsItemsKey";

    //region Actions & Extended Data Keys
    /**
     * Action который отсылается через sendBroadcast() в случае, когда новость
     * помечается как помещённая в раздел Избранное
     */
    public static final String NEWS_MARKED_AS_FAVORITE_ACTION =
            "com.samuelunknown.alfa_news.NEWS_MARKED_AS_FAVORITE_ACTION";

    /**
     * Action который отсылается через sendBroadcast() в случае, когда новость
     * помечается как удалённая из раздела Избранное
     */
    public static final String NEWS_MARKED_AS_NOT_FAVORITE_ACTION =
            "com.samuelunknown.alfa_news.NEWS_MARKED_AS_NOT_FAVORITE_ACTION";

    /**
     * Ключ для запаковывания DbNewsItem помеченного как Избранная/Не избранная новость
     */
    public static final String NEWS_MARKED_ITEM_EXTENDED_DATA_KEY =
            "com.samuelunknown.alfa_news.NEWS_MARKED_ITEM_EXTENDED_DATA_KEY";

    /**
     * Ключ для запаковывания индекса DbNewsItem помеченного как Избранная/Не избранная новость
     * под которым элемент находится в DbNewsItem в mViewPager
     */
    public static final String NEWS_MARKED_ITEM_INDEX_EXTENDED_DATA_KEY =
            "com.samuelunknown.alfa_news.NEWS_MARKED_ITEM_INDEX_EXTENDED_DATA_KEY";
    //endregion

    //endregion

    //region Variables
    /**
     * Список всех новостей
     */
    private List<DbNewsItem> mAllDbNewsItems;

    /**
     * Стек хранит порядок в котором открывались новости, инедксы новостей - это индексы
     * под которыми они находятся в mViewPager.
     */
    private Stack<Integer> mWebPagesIndexesStack;

    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    //endregion

    //region Main Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        DbNewsItem dbNewsItemWhatIsOpening;

        //region Intent
        Intent intent = getIntent();

        if (intent != null) {
            if (intent.hasExtra(INTENT_SELECTED_DB_NEWS_ITEM_KEY)) {
                String newsItemString = intent.getStringExtra(INTENT_SELECTED_DB_NEWS_ITEM_KEY);
                dbNewsItemWhatIsOpening = DbNewsItem.fromJson(newsItemString);
            } else {
                throw new NullPointerException("Intent hasn't INTENT_SELECTED_DB_NEWS_ITEM_KEY in " +
                        WebActivity.class);
            }

            if (intent.hasExtra(INTENT_ALL_DB_NEWS_ITEMS_KEY)) {

                List<String> allDbNewsItemsInJsonFormat = intent.getStringArrayListExtra(INTENT_ALL_DB_NEWS_ITEMS_KEY);
                mAllDbNewsItems = new ArrayList<>(allDbNewsItemsInJsonFormat.size());

                for (String itemInJsonFormat : allDbNewsItemsInJsonFormat) {
                    mAllDbNewsItems.add(DbNewsItem.fromJson(itemInJsonFormat));
                }
            } else {
                throw new NullPointerException("Intent hasn't INTENT_ALL_DB_NEWS_ITEMS_KEY in " +
                        WebActivity.class);
            }
        } else {
            throw new NullPointerException("Intent is null in " + WebActivity.class);
        }
        //endregion

        final int indexOfDbNewsItemWhatOpensFirst = mAllDbNewsItems.indexOf(dbNewsItemWhatIsOpening);

        mWebPagesIndexesStack = new Stack<>();

        //region Установка ViewPager and PagerAdapter.

        //region Небольшая заметка по поводу ViewPager'a
        // При открывании новой стрницы,
        // создаётся не один фрагмент под неё, а несколько, по умолчанию ещё 2
        // для страницы до и после открываемой (при условии если они есть, т.е. если открывается
        // не крайняя страница, или если она не одна единственная), делается это для плавности листания
        // количество это можно изменить через mViewPager.setOffscreenPageLimit(1 - минимум).
        //endregion

        mViewPager = (ViewPager) findViewById(R.id.view_pager_news);
        mViewPager.setOffscreenPageLimit(1);

        //region PagerAdapter
        mPagerAdapter = new WebSlidePagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        //endregion

        mViewPager.setCurrentItem(indexOfDbNewsItemWhatOpensFirst);

        //region OnPageChangeListener
        final ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                WebContract.FragmentView fragmentView =
                        (WebContract.FragmentView) mPagerAdapter.instantiateItem(mViewPager, position);

                mWebPagesIndexesStack.push(position);
                fragmentView.loadWebPage();
            }
        };

        mViewPager.addOnPageChangeListener(onPageChangeListener);
        //endregion

        //region Антибаг или Антифича
        // Суть в том, что при первом открытии этой активити с ViewPager'ом на борту,
        // не вызывается у ViewPager'а onPageSelected(int position), не смотря на то,
        // что страница выбирается и открывается. Кто-то считает это багом, кто-то фичей
        // см. тут -> https://stackoverflow.com/a/20292064/8009679
        // Вручную говорим что открывается страница с индексом indexOfDbNewsItemWhatOpensFirst,
        // делаем это через Runnable() только для того, что бы убедиться, что вызов произойдёт не
        // раньше, чем Fragment будет создан.
        mViewPager.post(new Runnable(){
            @Override
            public void run() {
                onPageChangeListener.onPageSelected(indexOfDbNewsItemWhatOpensFirst);
            }
        });
        //endregion

        //endregion

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, R.id.web_view_activity_toolbar);
        ToolBarUtils.setHomeAsUpIndicator(this, R.drawable.ic_close_black);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, true);
        ToolBarUtils.setDisplayShowHomeEnabled(this, true);
        //endregion

        //region StatusBar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion
    }

    @Override
    public void onBackPressed() {

        // выкидываем из стека текущую страницу, поскольку нам нужна пердыдущая
        mWebPagesIndexesStack.pop();

        if (mWebPagesIndexesStack.empty()) {
            super.onBackPressed();
        } else {
            // открываем предыдущую новость
            mViewPager.setCurrentItem(mWebPagesIndexesStack.pop());
        }
    }
    //endregion

    //region Implementation: WebContract.FragmentListener
    @Override
    public void onUpdateCurrentDbNewsItem(DbNewsItem dbNewsItem) {
        int index = mViewPager.getCurrentItem();
        mAllDbNewsItems.set(index, dbNewsItem);
        mPagerAdapter.notifyDataSetChanged();

        //region Send Broadcast что эта новость изменилась
        // (была добавлена в раздел Избранных или наоборот удалена из него)

        String action = NEWS_MARKED_AS_NOT_FAVORITE_ACTION;
        if (dbNewsItem.getFavoriteBoolean()) {
            action = NEWS_MARKED_AS_FAVORITE_ACTION;
        }

        Intent localIntent = new Intent(action).
                putExtra(
                        NEWS_MARKED_ITEM_EXTENDED_DATA_KEY,
                        DbNewsItem.toJson(dbNewsItem)).
                putExtra(
                        NEWS_MARKED_ITEM_INDEX_EXTENDED_DATA_KEY,
                        index);

        // Отправляем Intent для всех зарегистрированных слушателей в этом приложении
        // слушатель на самом деле один, это Main Activity, ему это нужно что бы обновить
        // этот элемент в RecyclerView
        sendBroadcast(localIntent);
        //endregion
    }

    @Override
    public void onShareWebPage(String link) {
        CommunicationUtils.shareLink(this, link);
    }
    //endregion

    private class WebSlidePagerAdapter extends FragmentStatePagerAdapter {

        WebSlidePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return WebFragment.newInstance(mAllDbNewsItems.get(position));
        }

        @Override
        public int getCount() {
            return mAllDbNewsItems.size();
        }
    }
}