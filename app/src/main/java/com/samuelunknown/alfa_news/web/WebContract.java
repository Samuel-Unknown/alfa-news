package com.samuelunknown.alfa_news.web;

import com.samuelunknown.alfa_news.news.DbNewsItem;

interface WebContract {

    interface View {
        void swapCurrentDbNewsItem(DbNewsItem dbNewsItem);
    }

    interface Presenter {
        void markNewsAsFavorite(DbNewsItem dbNewsItem, boolean favorite);
    }

    interface FragmentView {
        void loadWebPage();
        void stopLoadingWebPage();
    }

    interface FragmentListener {
        void onUpdateCurrentDbNewsItem(DbNewsItem dbNewsItem);
        void onShareWebPage(String link);
    }
}
