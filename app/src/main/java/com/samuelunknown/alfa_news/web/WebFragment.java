package com.samuelunknown.alfa_news.web;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.samuelunknown.alfa_news.R;
import com.samuelunknown.alfa_news.news.DbNewsItem;

import java.io.File;

public class WebFragment extends Fragment implements WebContract.View, WebContract.FragmentView {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = WebFragment.class.getSimpleName();

    /**
     * Строка является окончанием названия файлов в которых сохраняются веб-странички
     */
    private static final String WEB_ARCHIVE_FILE_NAME_SUFFIXES = "savedWebPage.mht";
    //endregion

    //region Arguments
    private static final String ARG_PARAM_DB_NEWS_ITEM = "paramDbNewsItem";
    private DbNewsItem mParamDbNewsItem;
    //endregion

    //region Variables
    private Context mContext;
    private Menu mMenu;

    private WebView mNewsWebView;
    private NewsWebViewClient mNewsWebViewClient;
    private ProgressDialog mProgressDialog;

    private WebContract.Presenter mPresenter;
    private WebContract.FragmentListener mListener;
    //endregion

    //region Create instance of WebFragment
    public WebFragment() {
        // Required empty public constructor
    }

    public static WebFragment newInstance(DbNewsItem paramDbNewsItem) {
        WebFragment fragment = new WebFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_DB_NEWS_ITEM, DbNewsItem.toJson(paramDbNewsItem));
        fragment.setArguments(args);
        return fragment;
    }
    //endregion

    //region Fragment Main Methods
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WebContract.FragmentListener) {
            mListener = (WebContract.FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDilemmaFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String newsItemString = getArguments().getString(ARG_PARAM_DB_NEWS_ITEM);
            mParamDbNewsItem = DbNewsItem.fromJson(newsItemString);
        } else {
            throw new NullPointerException("ARG_PARAM_DB_NEWS_ITEM is empty in " + WebFragment.class);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_web, container, false);

        mContext = getActivity();
        mPresenter = new WebPresenter(this, mContext);

        //region NewsWebView
        mNewsWebView = (WebView) view.findViewById(R.id.wv_news);

        //region WebSettings
        WebSettings settings = mNewsWebView.getSettings();
        settings.setJavaScriptEnabled(false);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);

        // Тут WebSettings.LOAD_NO_CACHE только для наглядности..
        // Что бы видеть разницу, что странички без интернета не загружаются, если
        // они не в разделе "Избранное".
        // А так конечно логичнее использовать WebSettings.LOAD_CACHE_ELSE_NETWORK
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        //endregion

        //region WebView ScrollBar Settings
        mNewsWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mNewsWebView.setScrollbarFadingEnabled(true);
        //endregion

        //region WebView Client
        mNewsWebViewClient = new NewsWebViewClient();

        mNewsWebView.setWebViewClient(mNewsWebViewClient);
        mNewsWebView.setWebChromeClient(new NewsWebChromeClient());
        //endregion

        //endregion

        return view;
    }

    //endregion

    //region Menu Options
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        inflater.inflate(R.menu.web_activity_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        updateMenuGroupVisible();
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();

        switch (itemThatWasClickedId) {
            case R.id.action_share:
                shareWebPage(mParamDbNewsItem.getLink());
                return true;
            case R.id.action_add_to_favorites:
                markWebNewsAsFavorite(true);
                return true;
            case R.id.action_remove_from_favorites:
                markWebNewsAsFavorite(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateMenuGroupVisible() {
        if (mMenu == null)
            return;

        boolean hasError = mNewsWebViewClient.hasLoadingError();

        mMenu.setGroupVisible(R.id.group_web_activity_common,
                !hasError);
        mMenu.setGroupVisible(R.id.group_web_activity_not_favorite_news,
                !hasError && !mParamDbNewsItem.getFavoriteBoolean());
        mMenu.setGroupVisible(R.id.group_web_activity_favorite_news,
                !hasError && mParamDbNewsItem.getFavoriteBoolean());
    }
    //endregion

    //region Implementation: WebContract.View
    @Override
    public void swapCurrentDbNewsItem(DbNewsItem dbNewsItem) {
        mParamDbNewsItem = dbNewsItem;
        mListener.onUpdateCurrentDbNewsItem(dbNewsItem);
        updateMenuGroupVisible();
    }
    //endregion

    //region Implementation: WebContract.FragmentView
    @Override
    public void loadWebPage() {
        if (mParamDbNewsItem.getFavoriteBoolean()) {
            mNewsWebViewClient.prepareToLoadUrl();
            mNewsWebView.loadUrl("file://" + getWebArchiveFileNameWithPath(mContext, mParamDbNewsItem));
        } else {
            mNewsWebViewClient.prepareToLoadUrl();
            mNewsWebView.loadUrl(mParamDbNewsItem.getLink());
        }
    }

    @Override
    public void stopLoadingWebPage() {
        mNewsWebView.loadUrl("about:blank");
        getActivity().invalidateOptionsMenu();
    }
    //endregion

    //region WebArchive File Name
    /**
     * Возвращает название файла в который сохраняется веб-страничка
     * @param item элемент на основании которого создаётся название
     */
    private String getWebArchiveFileName(DbNewsItem item) {
        String prefix = item.getIdString() + "_";
        return prefix + WEB_ARCHIVE_FILE_NAME_SUFFIXES;
    }

    /**
     * Возвращает название файла вместе с путём в который сохраняется веб-страничка
     * @param item элемент на основании которого создаётся название
     */
    private String getWebArchiveFileNameWithPath(@NonNull Context context, DbNewsItem item) {
        return context.getFilesDir().getPath() + getWebArchiveFileName(item);
    }
    //endregion

    //region Save & Delete WebArchive
    private void saveWebArchive(@NonNull String filename) {
        String webArchivePathFilename = mContext.getFilesDir().getPath() + filename;
        mNewsWebView.saveWebArchive(webArchivePathFilename);
    }

    private void deleteWebArchive(@NonNull String filenameWithPath) {
        File file = new File(filenameWithPath);

        boolean deleted = file.delete();

        if (!deleted) {
            Log.w(TAG, "WebArchive didn't delete! File name: " + filenameWithPath);
        }
    }
    //endregion

    private void markWebNewsAsFavorite(boolean favorite) {
        mPresenter.markNewsAsFavorite(mParamDbNewsItem, favorite);

        if (favorite) {
            saveWebArchive(getWebArchiveFileName(mParamDbNewsItem));
        } else {
            deleteWebArchive(getWebArchiveFileNameWithPath(mContext, mParamDbNewsItem));
        }
    }

    private void shareWebPage(String link) {
        mListener.onShareWebPage(link);
    }

    private class NewsWebViewClient extends WebViewClient {

        private boolean mHasLoadingError;

        boolean hasLoadingError() {
            return mHasLoadingError;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return false;
        }

        //region Заметка
        // При наличии ошибки загрузки страницы непонятно почему происходит
        // повторный вызов onPageStarted(), с последующим onPageFinished(),
        // поэтому в них есть проверка на наличие ошибки загрузки.
        //
        // Вот из-за этого ещё и добавлен prepareToLoadUrl(), для сброса флага наличия ошибки
        //endregion

        /**
         * Подготавливает NewsWebViewClient к загруке страницы, путём
         * сброса внутреннего состояния NewsWebViewClient
         */
        private void prepareToLoadUrl() {
            mHasLoadingError = false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (mHasLoadingError) {
                return;
            }
            initProgressDialog();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (mHasLoadingError) {
                return;
            }

            dismissProgressDialog();
            getActivity().invalidateOptionsMenu();
        }

        // Этот метод для отлавливания ошибки срабатывает только на устройствах версии
        // до Android Marshmallow
        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            mHasLoadingError = true;

            dismissProgressDialog();
            getActivity().invalidateOptionsMenu();
        }

        // Срабатывает только на устройствах версии начиная с Android Marshmallow, поэтому внутри
        // редирект на запрещённый метод описаннй выше
        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
            onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
        }

        private void initProgressDialog() {
            String loading = getString(R.string.news_loading_progress_text);

            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setTitle(null);
            mProgressDialog.setMessage(loading + " " + "0/100");
            mProgressDialog.setIndeterminate(true);

            // что бы при нажатии на экран не отменялась загрузка,
            // но её можно было отменить нажав на кнопку Back на устройстве
            mProgressDialog.setCancelable(true);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    mHasLoadingError = true;
                    stopLoadingWebPage();
                }
            });

            mProgressDialog.show();
        }

        private void dismissProgressDialog() {
            if (mProgressDialog != null && mProgressDialog.isShowing() && isAdded()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
    }

    private class NewsWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int progress) {
            if (mProgressDialog != null && mProgressDialog.isShowing() && isAdded()) {
                String loading = getString(R.string.news_loading_progress_text);
                mProgressDialog.setMessage(loading + " " + progress + "/100");
            }
        }
    }
}
