package com.samuelunknown.alfa_news.web;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.NonNull;

import com.samuelunknown.alfa_news.data.DbContract;
import com.samuelunknown.alfa_news.news.DbNewsItem;

import java.lang.ref.WeakReference;

class WebPresenter implements WebContract.Presenter {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = WebPresenter.class.getSimpleName();

    //region TOKENS
    private static final int UPDATE_NEWS_TOKEN_MARK_AS_FAVORITE = 100;
    private static final int UPDATE_NEWS_TOKEN_MARK_AS_NOT_FAVORITE = 200;
    //endregion

    //endregion

    //region Variables
    private WebContract.View mView;
    private NewsAsyncQueryHandler mAsyncQueryHandler;
    private DbNewsItem mDbNewsItem;
    //endregion

    WebPresenter(@NonNull WebContract.View view, @NonNull Context context) {
        mView = view;
        mAsyncQueryHandler = new NewsAsyncQueryHandler(this, context.getContentResolver());
    }

    private void updateNewsState(int updateToken, String newNewsState, String newsId) {

        if (!newNewsState.equals(DbContract.NewsEntry.VALUE_NEWS_IS_FAVORITE) &&
                !newNewsState.equals(DbContract.NewsEntry.VALUE_NEWS_IS_NOT_FAVORITE)) {
            throw new UnsupportedOperationException("updateNewsState(): unsupported newNewsState -> " + newNewsState);
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(
                DbContract.NewsEntry.COLUMN_FAVORITE,
                newNewsState);

        mAsyncQueryHandler.startUpdate(
                updateToken,
                null,
                DbContract.NewsEntry.CONTENT_URI,
                contentValues,
                DbContract.NewsEntry._ID + "=?",
                new String[] {newsId} );
    }

    //region Implementation: WebContract.Presenter
    @Override
    public void markNewsAsFavorite(DbNewsItem dbNewsItem, boolean favorite) {
        mDbNewsItem = dbNewsItem;
        if (favorite) {
            updateNewsState(
                    UPDATE_NEWS_TOKEN_MARK_AS_FAVORITE,
                    DbContract.NewsEntry.VALUE_NEWS_IS_FAVORITE,
                    dbNewsItem.getIdString());
        } else {
            updateNewsState(
                    UPDATE_NEWS_TOKEN_MARK_AS_NOT_FAVORITE,
                    DbContract.NewsEntry.VALUE_NEWS_IS_NOT_FAVORITE,
                    dbNewsItem.getIdString());
        }

    }
    //endregion

    //region Update Completed
    private void onUpdateCompleted(int token, int updatedRows) {
        switch (token) {
            //region case UPDATE_NEWS_TOKEN_MARK_AS_FAVORITE
            case UPDATE_NEWS_TOKEN_MARK_AS_FAVORITE: {
                if (updatedRows > 0) {
                    mView.swapCurrentDbNewsItem(DbNewsItem.replaceFavoriteProperty(mDbNewsItem, true));
                }
                break;
            }
            //endregion

            //region case UPDATE_NEWS_TOKEN_MARK_AS_NOT_FAVORITE
            case UPDATE_NEWS_TOKEN_MARK_AS_NOT_FAVORITE: {
                if (updatedRows > 0) {
                    mView.swapCurrentDbNewsItem(DbNewsItem.replaceFavoriteProperty(mDbNewsItem, false));
                }
                break;
            }
            //endregion
        }
    }
    //endregion

    private static class NewsAsyncQueryHandler extends AsyncQueryHandler {

        private final WeakReference<WebPresenter> mPresenterRef;

        NewsAsyncQueryHandler(WebPresenter presenter, ContentResolver cr) {
            super(cr);

            // берём weak reference что бы потом по завершении операций вставки,
            // обновления или удаления можно было вызвать методы класса WebPresenter,
            // если его экземпляр ещё не был удалён
            mPresenterRef = new WeakReference<>(presenter);
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            super.onUpdateComplete(token, cookie, result);

            WebPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onUpdateCompleted(token, result);
            }
        }
    }
}
