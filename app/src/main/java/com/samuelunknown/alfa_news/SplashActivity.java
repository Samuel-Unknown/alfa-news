package com.samuelunknown.alfa_news;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.samuelunknown.alfa_news.sync.NewsSyncTask;
import com.samuelunknown.alfa_news.sync.NewsSyncUtils;
import com.samuelunknown.alfa_news.utilities.StatusBarUtils;

public class SplashActivity extends AppCompatActivity {

    //region Variables
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = SplashActivity.class.getSimpleName();

    private NewsSyncStateReceiver mReceiver;
    private IntentFilter mReceiveIntentFilter;

    private boolean mFirstNewsSyncWasSuccessfully;
    //endregion

    //region Main Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mReceiver = new NewsSyncStateReceiver();

        mReceiveIntentFilter = new IntentFilter();
        mReceiveIntentFilter.addAction(NewsSyncTask.NEWS_SYNC_WAS_SUCCESSFULLY_ACTION);
        mReceiveIntentFilter.addAction(NewsSyncTask.NEWS_SYNC_WAS_UNSUCCESSFULLY_ACTION);

        mFirstNewsSyncWasSuccessfully = NewsSyncTask.getFirstNewsSyncWasSuccessfully(this);

        // Инициализируем загрузку новостей по графику
        NewsSyncUtils.initializeSyncDispatcher(this);

        //region StatusBar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion

        if (mFirstNewsSyncWasSuccessfully) {
            startMainActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // если приложение ещё ни разу не было синхронизировано
        if (!mFirstNewsSyncWasSuccessfully) {
            // то надо проверить пуста ли локальная БД и если пуста, то запустить немеделенную
            // синхронизацию, момент её завершения поймав с помощью BroadcastReceiver'а
            // поэтому сначала регистрируем ресивер
            registerReceiver(mReceiver, mReceiveIntentFilter);
            // запускаем процесс проверки БД
            new CheckDbIsEmptyAsyncTask(this).execute();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (!mFirstNewsSyncWasSuccessfully) {
            unregisterReceiver(mReceiver);
        }
    }
    //endregion

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * BroadcastReceiver для получения статуса обновления новостей.
     * <p>Подробнее тут:
     * <p>https://developer.android.com/training/run-background-service/report-status.html#ReportStatus
     * <p>https://classroom.udacity.com/courses/ud851/lessons/f5ef4e52-c485-4c85-a26a-3231c17d6154/concepts/44d8ed97-7091-45c1-b8ad-0f4dfd42c256
     */
    private class NewsSyncStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            boolean newsSyncUnsuccessfully = action.equals(NewsSyncTask.NEWS_SYNC_WAS_UNSUCCESSFULLY_ACTION);

            // Если не удалось синхронизироваться, показываем об этом сообщение
            if (newsSyncUnsuccessfully) {
                if (intent.hasExtra(NewsSyncTask.NEWS_SYNC_EXTENDED_DATA_STATUS_KEY)) {
                    String status = intent.getStringExtra(NewsSyncTask.NEWS_SYNC_EXTENDED_DATA_STATUS_KEY);

                    Toast.makeText(context, status, Toast.LENGTH_LONG).show();
                }
            } else {
                NewsSyncTask.setFirstNewsSyncWasSuccessfully(context);
            }

            startMainActivity();
        }
    }

    /**
     * AsyncTask для проверки наличия записей в БД, если их там нет то будет запущена синхронизация,
     * если они там есть, то откроется MainActivity, а Splash Screen будет закрыт
     */
    private class CheckDbIsEmptyAsyncTask extends AsyncTask<Void, Void, Boolean> {

        Context mContext;

        CheckDbIsEmptyAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return NewsSyncUtils.dbIsEmpty(mContext);
        }

        @Override
        protected void onPostExecute(Boolean dbIsEmpty) {
            if (dbIsEmpty) {
                NewsSyncUtils.startImmediateSync(mContext);
            } else {
                startMainActivity();
            }
        }
    }
}
