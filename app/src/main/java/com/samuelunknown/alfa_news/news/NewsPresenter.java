package com.samuelunknown.alfa_news.news;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.samuelunknown.alfa_news.data.DbContract;
import com.samuelunknown.alfa_news.sync.NewsSyncUtils;

public class NewsPresenter implements NewsContract.Presenter,
        LoaderManager.LoaderCallbacks<Cursor> {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = NewsPresenter.class.getSimpleName();

    //region MAIN_NEWS_PROJECTION
    private static final String[] MAIN_NEWS_PROJECTION = {
            DbContract.NewsEntry._ID,
            DbContract.NewsEntry.COLUMN_GUID,
            DbContract.NewsEntry.COLUMN_TITLE,
            DbContract.NewsEntry.COLUMN_LINK,
            DbContract.NewsEntry.COLUMN_PUB_DATE,
            DbContract.NewsEntry.COLUMN_FAVORITE,
            DbContract.NewsEntry.COLUMN_TIMESTAMP
    };

    // Если порядок строк в массиве MAIN_NEWS_PROJECTION изменится,
    // следует изменить и эти индексы
    public static final int INDEX_NEWS_ID = 0;
    public static final int INDEX_NEWS_GUID = 1;
    public static final int INDEX_NEWS_TITLE = 2;
    public static final int INDEX_NEWS_LINK = 3;
    public static final int INDEX_NEWS_PUT_DATE = 4;
    public static final int INDEX_NEWS_FAVORITE = 5;
    public static final int INDEX_NEWS_TIMESTAMP = 6;
    //endregion

    //region LOADERS ID
    // Уникальные ID для загрузчика данных из БД
    private static final int ALL_NEWS_LOADER_ID = 10;
    private static final int FAVORITE_NEWS_LOADER_ID = 11;
    //endregion
    //endregion

    //region Variables
    private NewsContract.View mView;
    private Context mContext;
    private LoaderManager mLoaderManager;
    //endregion

    NewsPresenter(@NonNull NewsContract.View view, @NonNull Context context,
                         @NonNull LoaderManager loaderManager) {
        mView = view;
        mContext = context;
        mLoaderManager = loaderManager;
    }

    //region Implementation: NewsContract.Presenter
    @Override
    public void loadAllNews() {
        Loader loader = mLoaderManager.getLoader(ALL_NEWS_LOADER_ID);
        if (loader != null) {
            mLoaderManager.restartLoader(ALL_NEWS_LOADER_ID, null, this);
        } else {
            mLoaderManager.initLoader(ALL_NEWS_LOADER_ID, null, this);
        }
    }

    @Override
    public void loadFavoriteNews() {
        Loader loader = mLoaderManager.getLoader(FAVORITE_NEWS_LOADER_ID);
        if (loader != null) {
            mLoaderManager.restartLoader(FAVORITE_NEWS_LOADER_ID, null, this);
        } else {
            mLoaderManager.initLoader(FAVORITE_NEWS_LOADER_ID, null, this);
        }
    }

    @Override
    public void refreshDbNewsItems() {
        NewsSyncUtils.startImmediateSync(mContext);
    }
    //endregion

    //region Cursor Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        mView.showLoadingProcess();

        switch (id) {
            case ALL_NEWS_LOADER_ID:
                return new CursorLoader(mContext, DbContract.NewsEntry.CONTENT_URI,
                        MAIN_NEWS_PROJECTION, null, null,
                        DbContract.NewsEntry.COLUMN_PUB_DATE + " DESC");

            case FAVORITE_NEWS_LOADER_ID:
                return new CursorLoader(mContext, DbContract.NewsEntry.CONTENT_URI,
                        MAIN_NEWS_PROJECTION, DbContract.NewsEntry.COLUMN_FAVORITE + "=?",
                        new String[]{DbContract.NewsEntry.VALUE_NEWS_IS_FAVORITE},
                        DbContract.NewsEntry.COLUMN_PUB_DATE + " DESC");

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mView.swapNews(data);
        mView.hideLoadingProcess();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mView.swapNews(null);
    }
    //endregion
}
