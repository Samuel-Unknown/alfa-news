package com.samuelunknown.alfa_news.news;

import com.google.gson.Gson;
import com.samuelunknown.alfa_news.data.DbContract;

public class DbNewsItem extends RssNewsItem {
    private final long mId;
    private final int mFavorite;
    private final long mTimeStamp;

    public DbNewsItem(final long id, final String guid,
                      final String title, final String link,
                      final long putDate, final int favorite,
                      final long timeStamp) {
        super(guid, title, link, putDate);

        mId = id;
        mFavorite = favorite;
        mTimeStamp = timeStamp;
    }

    public long getId() {
        return mId;
    }

    public String getIdString() {
        return String.valueOf(mId);
    }

    public int getFavorite() {
        return mFavorite;
    }

    public String getFavoriteString() {
        return String.valueOf(mFavorite);
    }

    public boolean getFavoriteBoolean() {
        return getFavoriteString().equals(DbContract.NewsEntry.VALUE_NEWS_IS_FAVORITE);
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }

    public static String toJson(DbNewsItem dbNewsItem) {
        return new Gson().toJson(dbNewsItem);
    }

    public static DbNewsItem fromJson(String dbNewsItemString) {
        return new Gson().fromJson(dbNewsItemString, DbNewsItem.class);
    }

    /**
     * Возвращает копию DbNewsItem заменив в нем свойство favorite на заданное
     * @param item исходный DbNewsItem
     * @param favorite новое значение для favorite свйоства у DbNewsItem
     */
    public static DbNewsItem replaceFavoriteProperty(DbNewsItem item, boolean favorite) {
        int favoriteInt;

        if (favorite) {
            favoriteInt = Integer.valueOf(DbContract.NewsEntry.VALUE_NEWS_IS_FAVORITE);
        } else {
            favoriteInt = Integer.valueOf(DbContract.NewsEntry.VALUE_NEWS_IS_NOT_FAVORITE);
        }

        return new DbNewsItem(
                item.getId(), item.getGuid(), item.getTitle(),
                item.getLink(), item.getPubDate(), favoriteInt, item.getTimeStamp());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DbNewsItem that = (DbNewsItem) o;

        if (mId != that.mId) return false;
        if (mFavorite != that.mFavorite) return false;
        return mTimeStamp == that.mTimeStamp;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (mId ^ (mId >>> 32));
        result = 31 * result + mFavorite;
        result = 31 * result + (int) (mTimeStamp ^ (mTimeStamp >>> 32));
        return result;
    }
}
