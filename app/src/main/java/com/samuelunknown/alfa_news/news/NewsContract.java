package com.samuelunknown.alfa_news.news;

import android.database.Cursor;

import java.util.List;

public interface NewsContract {
    interface View {
        void showLoadingProcess();
        void hideLoadingProcess();

        void swapNews(Cursor cursor);
    }

    interface Presenter {
        void loadAllNews();
        void loadFavoriteNews();

        // вызывает процесс обновления данных в локальной БД
        void refreshDbNewsItems();
    }

    interface FragmentView {
        List<DbNewsItem> getAllDbNewsItems();
        void swapItem(int index, DbNewsItem item);
    }

    interface FragmentListener {
        void onOpenNews(DbNewsItem selectedDbNewsItem);
    }
}