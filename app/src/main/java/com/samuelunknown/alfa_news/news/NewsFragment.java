package com.samuelunknown.alfa_news.news;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.samuelunknown.alfa_news.R;
import com.samuelunknown.alfa_news.adapters.NewsListAdapter;
import com.samuelunknown.alfa_news.sync.NewsSyncTask;

import java.util.List;

public class NewsFragment extends Fragment
        implements
        NewsContract.FragmentView,
        NewsContract.View,
        NewsListAdapter.NewsListAdapterListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = NewsFragment.class.getSimpleName();

    //region Arguments
    private static final String ARG_IS_FAVORITE_SECTION_REPRESENTATION = "paramIsFavoriteSectionRepresentation";

    private boolean mParamIsFavoriteSectionRepresentation;
    //endregion

    //region Variables
    private NewsContract.Presenter mPresenter;
    private NewsListAdapter mNewsListAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mNewsListRecyclerView;
    private ProgressBar mProgressBar;
    private Context mContext;

    private NewsSyncStateReceiver mReceiver;
    private IntentFilter mReceiveIntentFilter;
    /**
     * Идёт ли сейчас процесс обновления новостей
     */
    private boolean mNewsIsRefreshing;

    private NewsContract.FragmentListener mFragmentListener;
    //endregion

    //region Create instance of NewsFragment
    public NewsFragment() {
        // Required empty public constructor
    }

    public static NewsFragment newInstance(boolean paramIsFavoriteSectionRepresentation) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_IS_FAVORITE_SECTION_REPRESENTATION, paramIsFavoriteSectionRepresentation);
        fragment.setArguments(args);
        return fragment;
    }
    //endregion

    //region Fragment Main Methods
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NewsContract.FragmentListener) {
            mFragmentListener = (NewsContract.FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NewsContract.FragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamIsFavoriteSectionRepresentation =
                    getArguments().getBoolean(ARG_IS_FAVORITE_SECTION_REPRESENTATION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_news, container, false);
        mContext = getActivity();

        mNewsListRecyclerView = (RecyclerView) view.findViewById(R.id.rv_news);
        mNewsListRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mNewsListRecyclerView.setHasFixedSize(true);

        mNewsListAdapter = new NewsListAdapter(mContext, this);
        mNewsListRecyclerView.setAdapter(mNewsListAdapter);

        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_news);

        mReceiveIntentFilter = new IntentFilter();
        mReceiveIntentFilter.addAction(NewsSyncTask.NEWS_SYNC_WAS_SUCCESSFULLY_ACTION);
        mReceiveIntentFilter.addAction(NewsSyncTask.NEWS_SYNC_WAS_UNSUCCESSFULLY_ACTION);

        mPresenter = new NewsPresenter(this, mContext, getLoaderManager());

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srl_news);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startRefreshing();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mReceiver = new NewsSyncStateReceiver();
        getActivity().registerReceiver(mReceiver, mReceiveIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadItems();
    }
    //endregion

    //region Implementation: NewsContract.FragmentView
    @Override
    public List<DbNewsItem> getAllDbNewsItems() {
        return mNewsListAdapter.getAllDbNewsItems();
    }

    @Override
    public void swapItem(int index, DbNewsItem item) {
        mNewsListAdapter.swapDbNewsItem(index, item);
    }
    //endregion

    //region Implementation: NewsContract.View
    @Override
    public void showLoadingProcess() {
        mProgressBar.setVisibility(View.VISIBLE);
        mNewsListRecyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideLoadingProcess() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mNewsListRecyclerView.setVisibility(View.VISIBLE);

        // если это был процесс когда новости обновлили вручную, то завершаем его
        if (mNewsIsRefreshing) {
            Toast.makeText(mContext, R.string.news_refreshed, Toast.LENGTH_LONG).show();
            stopRefreshing();
        }
    }

    @Override
    public void swapNews(Cursor cursor) {
        mNewsListAdapter.swapCursor(cursor);
    }
    //endregion

    //region Implementation: NewsListAdapter.NewsListAdapterListener
    @Override
    public void onNewsClick(int position) {
        DbNewsItem selectedItem = mNewsListAdapter.getDbNewsItem(position);
        mFragmentListener.onOpenNews(selectedItem);
    }
    //endregion

    private void startRefreshing() {
        mPresenter.refreshDbNewsItems();
        mNewsIsRefreshing = true;
    }

    private void stopRefreshing() {
        mSwipeRefreshLayout.setRefreshing(false);
        mNewsIsRefreshing = false;
    }

    private void loadItems() {
        if (mParamIsFavoriteSectionRepresentation) {
            mPresenter.loadFavoriteNews();
        } else {
            mPresenter.loadAllNews();
        }
    }

    private class NewsSyncStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            boolean newsSyncUnsuccessfully = action.equals(NewsSyncTask.NEWS_SYNC_WAS_UNSUCCESSFULLY_ACTION);

            if (newsSyncUnsuccessfully) {
                // Не удалось синхронизироваться, показываем об этом сообщение
                if (intent.hasExtra(NewsSyncTask.NEWS_SYNC_EXTENDED_DATA_STATUS_KEY)) {
                    String status = intent.getStringExtra(NewsSyncTask.NEWS_SYNC_EXTENDED_DATA_STATUS_KEY);

                    Toast.makeText(context, status, Toast.LENGTH_LONG).show();
                }

                // если это был процесс когда новости обновлили вручную, то завершаем его
                if (mNewsIsRefreshing) {
                    stopRefreshing();
                }
            } else {
                // Удалось синхронизироваться, загружаем новости из БД
                loadItems();
            }
        }
    }
}
