package com.samuelunknown.alfa_news.news;

public class RssNewsItem {
    private final String mGuid;
    private final String mTitle;
    private final String mLink;
    private final long mPubDate;

    public RssNewsItem(final String guid,
                       final String title, final String link,
                       final long pubDate) {
        mGuid = guid;
        mTitle = title;
        mLink = link;
        mPubDate = pubDate;
    }

    public String getGuid() {
        return mGuid;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getLink() {
        return mLink;
    }

    public long getPubDate() {
        return mPubDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RssNewsItem that = (RssNewsItem) o;

        if (mPubDate != that.mPubDate) return false;
        if (!mGuid.equals(that.mGuid)) return false;
        if (!mTitle.equals(that.mTitle)) return false;
        return mLink.equals(that.mLink);

    }

    @Override
    public int hashCode() {
        int result = mGuid.hashCode();
        result = 31 * result + mTitle.hashCode();
        result = 31 * result + mLink.hashCode();
        result = 31 * result + (int) (mPubDate ^ (mPubDate >>> 32));
        return result;
    }
}