package com.samuelunknown.alfa_news.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

public class NewsFirebaseJobService extends JobService {

    private AsyncTask<Void, Void, Void> mFetchNewsTask;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        mFetchNewsTask = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                Context context = getApplicationContext();
                NewsSyncTask.syncNews(context);
                jobFinished(jobParameters, false);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                jobFinished(jobParameters, false);
            }
        };

        mFetchNewsTask.execute();

        return true; // Продолжается ли работа?
                     // (true, т.к. работа запущена в отдельном потоке в методе doInBackground())
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (mFetchNewsTask != null) {
            mFetchNewsTask.cancel(true);
        }
        return true; // Должны ли мы попробовать снова запустить работу если вдруг она прервалась?
                     // (например если пропал доступ в интернет)
    }
}
