package com.samuelunknown.alfa_news.sync;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

public class NewsSyncIntentService extends IntentService {

    public NewsSyncIntentService() {
        super(NewsSyncIntentService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        NewsSyncTask.syncNews(this);
    }
}