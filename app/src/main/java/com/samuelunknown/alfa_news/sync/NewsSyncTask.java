package com.samuelunknown.alfa_news.sync;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.samuelunknown.alfa_news.R;
import com.samuelunknown.alfa_news.data.DbContract;
import com.samuelunknown.alfa_news.news.RssNewsItem;
import com.samuelunknown.alfa_news.utilities.AppDateUtils;
import com.samuelunknown.alfa_news.utilities.NetworkUtils;
import com.samuelunknown.alfa_news.utilities.RssParserUtils;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

public class NewsSyncTask {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = NewsSyncTask.class.getSimpleName();

    private static final String ALFA_BANK_RSS_BASE_URL = "https://alfabank.ru/retail/archive/rss/";

    //region Actions & Extended Data Key
    public static final String NEWS_SYNC_WAS_SUCCESSFULLY_ACTION =
            "com.samuelunknown.alfa_news.NEWS_SYNC_WAS_SUCCESSFULLY_ACTION";

    public static final String NEWS_SYNC_WAS_UNSUCCESSFULLY_ACTION =
            "com.samuelunknown.alfa_news.NEWS_SYNC_WAS_UNSUCCESSFULLY_ACTION";

    /**
     * Ключ для вытаскивания из Intent'а статуса синхронизации после её завершения
     */
    public static final String NEWS_SYNC_EXTENDED_DATA_STATUS_KEY =
            "com.samuelunknown.alfa_news.NEWS_SYNC_EXTENDED_DATA_STATUS_KEY";
    //endregion

    //region Shared Preference
    private static final String SHARED_PREFERENCE_FILE_KEY =
            "com.samuelunknown.alfa_news.SHARED_PREFERENCE_FILE_KEY";

    private static final String FIRST_NEWS_SYNC_WAS_SUCCESSFUL_SHARED_PREFERENCE_KEY =
            "FIRST_NEWS_SYNC_WAS_SUCCESSFUL_SHARED_PREFERENCE_KEY";
    //endregion
    //endregion

    /**
     * Синхронизирует состояние локальной БД с данными полученными через RSS-канал
     * @param context контекст
     */
    synchronized static void syncNews(Context context) {
        URL url = NetworkUtils.buildURL(ALFA_BANK_RSS_BASE_URL);

        if (url != null) {
            try {
                InputStream inputStream = NetworkUtils.getRssData(url);

                // сюда никогда не передастья пустой inputStream, так как если данные
                // не удалось получить выше, то там будет exception
                List<RssNewsItem> list = RssParserUtils.parse(inputStream);

                if (list.size() != 0) {
                    ContentResolver contentResolver = context.getContentResolver();
                    //region Удаление старых новостей из БД
                    String selectionStatement = DbContract.NewsEntry.getSqlSelectForNotFavoriteNews();

                    contentResolver.delete(
                            DbContract.NewsEntry.CONTENT_URI,
                            selectionStatement,
                            null);
                    //endregion

                    //region Добавление новых новостей в БД
                    ContentValues[] contentValues = new ContentValues[list.size()];

                    for (int i = 0; i < list.size(); i++) {
                        contentValues[i] = new ContentValues();
                        contentValues[i].put(DbContract.NewsEntry.COLUMN_GUID, list.get(i).getGuid());
                        contentValues[i].put(DbContract.NewsEntry.COLUMN_TITLE, list.get(i).getTitle());
                        contentValues[i].put(DbContract.NewsEntry.COLUMN_LINK, list.get(i).getLink());
                        contentValues[i].put(DbContract.NewsEntry.COLUMN_PUB_DATE, list.get(i).getPubDate());
                        contentValues[i].put(DbContract.NewsEntry.COLUMN_FAVORITE, DbContract.NewsEntry.VALUE_NEWS_IS_NOT_FAVORITE);
                        contentValues[i].put(DbContract.NewsEntry.COLUMN_TIMESTAMP, AppDateUtils.getGmtForCurrentTime());
                    }

                    contentResolver.bulkInsert(
                            DbContract.NewsEntry.CONTENT_URI,
                            contentValues);
                    //endregion

                    Intent localIntent =
                            new Intent(NEWS_SYNC_WAS_SUCCESSFULLY_ACTION).putExtra(
                                    NEWS_SYNC_EXTENDED_DATA_STATUS_KEY,
                                    R.string.news_sync_was_successfully);

                    // Отправляем Intent для всех зарегистрированных слушателей в этом приложении
                    context.sendBroadcast(localIntent);

//                    Log.d(TAG, "* СИНХРОНИЗИРОВАЛИСЬ *");
                }
            } catch (Exception e) {
                // Возможно не доступен сервер
                e.printStackTrace();

                Intent localIntent =
                        new Intent(NEWS_SYNC_WAS_UNSUCCESSFULLY_ACTION).putExtra(
                                NEWS_SYNC_EXTENDED_DATA_STATUS_KEY,
                                R.string.news_sync_was_unsuccessfully + ": " + e.getMessage());

                // Отправляем Intent для всех зарегистрированных слушателей в этом приложении
                context.sendBroadcast(localIntent);
            }
        }
    }

    /**
     * Сохраняет информацию в SharedPreferences о том что первая синхронизация прошла успешно.
     */
    public static void setFirstNewsSyncWasSuccessfully(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                SHARED_PREFERENCE_FILE_KEY,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(FIRST_NEWS_SYNC_WAS_SUCCESSFUL_SHARED_PREFERENCE_KEY, true);
        editor.apply();
    }

    /**
     * Возвращает true если первая синхронизация прошла успешно.
     */
    public static boolean getFirstNewsSyncWasSuccessfully(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                SHARED_PREFERENCE_FILE_KEY,
                Context.MODE_PRIVATE);

        return sharedPref.getBoolean(FIRST_NEWS_SYNC_WAS_SUCCESSFUL_SHARED_PREFERENCE_KEY, false);
    }
}
