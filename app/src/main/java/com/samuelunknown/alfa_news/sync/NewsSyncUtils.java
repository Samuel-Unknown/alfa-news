package com.samuelunknown.alfa_news.sync;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;
import com.samuelunknown.alfa_news.data.DbContract;

import java.util.concurrent.TimeUnit;

public class NewsSyncUtils {
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = NewsSyncUtils.class.getSimpleName();

    private static boolean sInitialized;
    // синхронизация происходит каждые 10-15 минут
    private static final int SYNC_INTERVAL_MINUTES = 10;
    private static final int SYNC_FLEXTIME_MINUTES = 15;
    private static final int SYNC_INTERVAL_SECONDS = (int) TimeUnit.MINUTES.toSeconds(SYNC_INTERVAL_MINUTES);
    private static final int SYNC_FLEXTIME_SECONDS = (int) TimeUnit.MINUTES.toSeconds(SYNC_FLEXTIME_MINUTES);

    private static final String NEWS_SYNC_TAG = "news-sync-tag";

    private static void scheduleFirebaseJobDispatcherSync(@NonNull final Context context) {

        Driver driver = new GooglePlayDriver(context);
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(driver);

        Job syncNewsJob = dispatcher.newJobBuilder()
                .setService(NewsFirebaseJobService.class)
                .setTag(NEWS_SYNC_TAG)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                // setLifetime() устанавливает как долго будет существовать наш Job. Текущее
                // значение говорит о том, что даже после перезагрузки устройтсва Job будет
                // существовать.
                .setLifetime(Lifetime.FOREVER)
                // Мы хотим что бы новости обновлялись постоянно, поэтому true
                .setRecurring(true)
                // Мы хотим что бы данные о последних новостях обновлялись каждые 10-15 минут
                .setTrigger(Trigger.executionWindow(
                        SYNC_INTERVAL_SECONDS,
                        SYNC_FLEXTIME_SECONDS))
                .setReplaceCurrent(true)
                .build();


        dispatcher.schedule(syncNewsJob);
    }

    synchronized public static void initializeSyncDispatcher(@NonNull final Context context) {

        // Инициализация выполняется лишь раз за время жизни приложения
        if (sInitialized) {
            return;
        }

        sInitialized = true;

        // Этот метод создаёт задачу переодической синхронизации новостей
        scheduleFirebaseJobDispatcherSync(context);
    }

    public static void startImmediateSync(@NonNull final Context context) {
        Intent intentToSyncImmediately = new Intent(context, NewsSyncIntentService.class);
        context.startService(intentToSyncImmediately);
    }

    public static boolean dbIsEmpty(@NonNull final Context context) {

        //Проверка БД на наличие новостей полученных ранее при синхронизации
        Uri forecastQueryUri = DbContract.NewsEntry.CONTENT_URI;

        // Для проверки наличия данных получаем только те новости из БД, которые не были
        // помечены как избранные
        String[] projectionColumns = {DbContract.NewsEntry._ID};
        String selectionStatement = DbContract.NewsEntry.getSqlSelectForNotFavoriteNews();

        // Получаем данные из БД что бы убедиться что они там есть
        Cursor cursor = context.getContentResolver().query(
                forecastQueryUri,
                projectionColumns,
                selectionStatement,
                null,
                null);

        
        boolean isEmpty = false;

        // Объект Cursor может быть null по разным причинам, некоторые приведены ниже:
        //   1) Неправильный URI
        //   2) query() метод ContentProvider'а возвращает null
        //   3) Произощёл RemoteException
        //
        // Если Cursor оказался null или он пуст
        if (cursor == null|| cursor.getCount() == 0) {
            isEmpty = true;
        }

        // Что бы не было утечек памяти закрываем Cursor
        if (cursor != null) {
            cursor.close();
        }

        return isEmpty;
    }

}
