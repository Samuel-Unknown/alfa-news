package com.samuelunknown.alfa_news.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.samuelunknown.alfa_news.data.DbContract.*;

/**
 * Класс для управления локальной БД
 */
public class NewsDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "alfa_news.db";

    /**
     * Контекст приложения
     */
    private Context mContext;

    /**
     * Версия БД приложения.
     * Каждый раз внося изменения в структуру БД, следует увеличивать DATABASE_VERSION на 1.
     * Первая версия имеет номер = 1.
     * Кроме того следует добавить метод апгрейда до новой версии БД в onUpgrade().
     */
    private static final int DATABASE_VERSION = 1;

    public NewsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context.getApplicationContext();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_DILEMMA_TABLE =
                "CREATE TABLE " + NewsEntry.TABLE_NAME + " (" +

                        NewsEntry._ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, "          +
                        NewsEntry.COLUMN_GUID       + " TEXT NOT NULL UNIQUE, "                       +
                        NewsEntry.COLUMN_TITLE      + " TEXT NOT NULL, "                              +
                        NewsEntry.COLUMN_LINK       + " TEXT NOT NULL, "                              +
                        NewsEntry.COLUMN_PUB_DATE   + " INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                        NewsEntry.COLUMN_FAVORITE   + " INTEGER NOT NULL DEFAULT 0, "                 +
                        NewsEntry.COLUMN_TIMESTAMP  + " INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP "  +
                        ");";

        db.execSQL(SQL_CREATE_DILEMMA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Последовательный апгрейд БД
        // 1 -> 2 -> 3-> ... -> n
        switch (oldVersion) {
            default:
                db.execSQL("DROP TABLE IF EXISTS " + NewsEntry.TABLE_NAME);
                onCreate(db);
        }
    }

}
