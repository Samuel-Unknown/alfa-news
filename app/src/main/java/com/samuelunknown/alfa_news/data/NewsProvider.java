package com.samuelunknown.alfa_news.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class NewsProvider extends ContentProvider {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = NewsProvider.class.getSimpleName();

    // Эти константы будут использованы для сопоставления URI и данных на которые URI указывает
    public static final int CODE_NEWS = 100;
    public static final int CODE_NEWS_WITH_ID = 101;

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    //endregion

    private NewsDbHelper mNewsDbHelper;

    public static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DbContract.CONTENT_AUTHORITY;

        // Этот URI -> content://com.samuelunknown.alfa_news/news
        matcher.addURI(authority, DbContract.PATH_NEWS, CODE_NEWS);

        // Этот URI -> content://com.samuelunknown.alfa_news/news/131
        // (131 выбранно случайно, тут может быть любое число)
        matcher.addURI(authority, DbContract.PATH_NEWS + "/#", CODE_NEWS_WITH_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mNewsDbHelper = new NewsDbHelper(getContext());
        return true;
    }

    // Параметры selection и selectionArgs игнорируются для Uri ссылающихся на конкретную новость
    // Пример Uri с сылкой на коретную новость: content://com.samuelunknown.alfa_news/news/131
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final SQLiteDatabase db = mNewsDbHelper.getReadableDatabase();

        Cursor retCursor;

        switch (sUriMatcher.match(uri)) {
            case CODE_NEWS: {

                retCursor = db.query(DbContract.NewsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            case CODE_NEWS_WITH_ID: {
                String id_news = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    @SuppressWarnings({"UnusedDeclaration"})
                    long id = Long.valueOf(id_news);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong news id: " + id_news);
                }
                //endregion

                retCursor = db.query(DbContract.NewsEntry.TABLE_NAME,
                        projection,
                        DbContract.NewsEntry._ID + "=?",
                        new String[]{id_news},
                        null,
                        null,
                        null);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in query()");
        }

        retCursor.setNotificationUri(context.getContentResolver(), uri);

        return retCursor;
    }

    // Uri должны ссылаться на таблицу с новостями (content://com.samuelunknown.alfa_news/news)
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        final SQLiteDatabase db = mNewsDbHelper.getWritableDatabase();

        Uri returnUri;

        switch (sUriMatcher.match(uri)) {
            case CODE_NEWS: {
                long id = db.insert(DbContract.NewsEntry.TABLE_NAME, null, values);
                if ( id > 0 ) {
                    returnUri = ContentUris.withAppendedId(DbContract.NewsEntry.CONTENT_URI, id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in insert()");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return returnUri;
    }

    // Uri должны ссылаться на таблицу с новостями (content://com.samuelunknown.alfa_news/news)
    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = mNewsDbHelper.getWritableDatabase();

        int insertedRows = 0;

        switch (sUriMatcher.match(uri)) {
            case CODE_NEWS: {
                db.beginTransaction();
                try {
                    for (ContentValues cv : values) {
                        try {
                            db.insertOrThrow(DbContract.NewsEntry.TABLE_NAME, null, cv);
                            insertedRows++;
                        } catch(SQLiteConstraintException e) {
                            // Если не удалось вставить запись из-за того, что такая новость в БД
                            // уже есть, то игнорируем, её и не надо вставлять ибо она там есть.
                            // Для таблицы новостей этот эксепшн может возникнуть либо когда
                            // ID записи совпадает или GUID столбец совпадает, на последнее как раз
                            // и срабатывает catch блок и мы попадаем сюда. При этом, это имеет
                            // смысл только для добавленных в избранное новостей, т.к. все
                            // неизбранные удаляются перед тем, как мы вставляем новые
                            // используя bulkInsert()
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in bulkInsert()");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return insertedRows;
    }

    // Параметры selection и selectionArgs игнорируются для Uri ссылающихся на конкретную новость
    // Пример Uri с сылкой на коретную новость: content://com.samuelunknown.alfa_news/news/131
    // При ссылке без конкретики данные из таблицы удаляются целиком
    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mNewsDbHelper.getWritableDatabase();

        int numRowsDeleted;

        switch (sUriMatcher.match(uri)) {
            case CODE_NEWS: {
                // удаляем новости из БД согласно selection и selectionArgs
                numRowsDeleted = db.delete(
                        DbContract.NewsEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
                break;
            }
            case CODE_NEWS_WITH_ID: {
                String id_news = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    @SuppressWarnings({"UnusedDeclaration"})
                    long id = Long.valueOf(id_news);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong news id: " + id_news);
                }
                //endregion

                // удаляем новость с коретным _ID
                numRowsDeleted = db.delete(
                        DbContract.NewsEntry.TABLE_NAME,
                        DbContract.NewsEntry._ID + "=?",
                        new String[]{id_news});
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }


        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in delete()");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return numRowsDeleted;
    }

    // Параметры selection и selectionArgs игнорируются для Uri ссылающихся на конкретную новость
    // Пример Uri с сылкой на коретную новость: content://com.samuelunknown.alfa_news/news/131
    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        final SQLiteDatabase db = mNewsDbHelper.getWritableDatabase();

        int numRowsUpdated;

        switch (sUriMatcher.match(uri)) {
            case CODE_NEWS: {
                // этот вариант используется когда разом помечаются новости (например как избранные)
                numRowsUpdated = db.update(
                        DbContract.NewsEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            }
            case CODE_NEWS_WITH_ID:
                String id_news = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    @SuppressWarnings({"UnusedDeclaration"})
                    long id = Long.valueOf(id_news);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong news id: " + id_news);
                }
                //endregion

                numRowsUpdated = db.update(
                        DbContract.NewsEntry.TABLE_NAME,
                        values,
                        DbContract.NewsEntry._ID + "=?",
                        new String[]{id_news});

                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in update()");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return numRowsUpdated;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        throw new RuntimeException(
                "We are not implementing getType.");
    }
}
