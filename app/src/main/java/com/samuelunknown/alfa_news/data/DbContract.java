package com.samuelunknown.alfa_news.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Класс определяющий таблицы и имена столбцов в базе данных.
 */
public class DbContract {

    public static final String CONTENT_AUTHORITY = "com.samuelunknown.alfa_news";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_NEWS = "news";

    /**
     * Таблица Новостей
     */
    public static final class NewsEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_NEWS)
                .build();

        //*********************** Таблица ***********************//
        public static final String TABLE_NAME = "news";
        //*********************** Колонки ***********************//
        // TEXT UNIQUE
        public static final String COLUMN_GUID   = "guid";
        // TEXT
        public static final String COLUMN_TITLE  = "title";
        // TEXT
        public static final String COLUMN_LINK  = "link";
        // INTEGER
        public static final String COLUMN_PUB_DATE = "pub_date";
        // INTEGER enum (0 - news isn't favorite, 1 - news is favorite), default = 0
        public static final String COLUMN_FAVORITE = "favorite";
        // INTEGER
        public static final String COLUMN_TIMESTAMP = "timestamp";
        //********************************************************//

        // значения для колонки COLUMN_FAVORITE
        public static final String VALUE_NEWS_IS_FAVORITE = "1";
        public static final String VALUE_NEWS_IS_NOT_FAVORITE   = "0";

        public static String getSqlSelectForNotFavoriteNews() {
            return DbContract.NewsEntry.COLUMN_FAVORITE + "=" +
                    DbContract.NewsEntry.VALUE_NEWS_IS_NOT_FAVORITE;
        }
    }
}
