package com.samuelunknown.alfa_news.about;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.samuelunknown.alfa_news.R;
import com.samuelunknown.alfa_news.utilities.CommunicationUtils;
import com.samuelunknown.alfa_news.utilities.StatusBarUtils;
import com.samuelunknown.alfa_news.utilities.ToolBarUtils;

public class AboutActivity extends AppCompatActivity implements AboutContract.FragmentListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = AboutActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, R.id.about_activity_toolbar);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, true);
        ToolBarUtils.setDisplayShowHomeEnabled(this, true);
        //endregion

        //region StatusBar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion

        addAboutFragment();
    }

    private void addAboutFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        AboutFragment aboutFragment =
                (AboutFragment) fragmentManager.findFragmentById(R.id.about_fragment_container);

        if (aboutFragment == null) {
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.about_fragment_container, new AboutFragment())
                    .commit();
        }
    }

    private void openWriteToDeveloper() {
        CommunicationUtils.openSendMailToDeveloper(this);
    }

    //region Implementation: AboutContract.FragmentListener
    @Override
    public void onWriteToDeveloper() {
        openWriteToDeveloper();
    }
    //endregion
}
