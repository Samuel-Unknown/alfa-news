package com.samuelunknown.alfa_news.about;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.samuelunknown.alfa_news.R;

public class AboutFragment extends Fragment {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = AboutFragment.class.getSimpleName();

    private AboutContract.FragmentListener mFragmentListener;

    public AboutFragment() {
        // Required empty public constructor
    }

    //region Fragment Main Methods
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AboutContract.FragmentListener) {
            mFragmentListener = (AboutContract.FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement AboutContract.FragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_about, container, false);

        Button button = (Button) view.findViewById(R.id.b_write_to_developer);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentListener.onWriteToDeveloper();
            }
        });

        return view;
    }
    //endregion
}