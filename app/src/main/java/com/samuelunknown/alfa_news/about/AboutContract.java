package com.samuelunknown.alfa_news.about;

interface AboutContract {

    interface FragmentListener {
        void onWriteToDeveloper();
    }
}
