package com.samuelunknown.alfa_news.utilities;

import android.util.Xml;

import com.samuelunknown.alfa_news.news.RssNewsItem;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Класс для парсинга RSS 2.0
 *
 * <p>Позволяет получить List элементов представляющих из себя item'ы постов
 *
 * <p>https://cyber.harvard.edu/rss/rss.html
 *
 */
public class RssParserUtils {

    //region Const Values
    private static final String TAG_RSS        = "rss";
    private static final String TAG_CHANNEL    = "channel";
    private static final String TAG_ITEM       = "item";

    private static final String TAG_GUID       = "guid";
    private static final String TAG_TITLE      = "title";
    private static final String TAG_LINK       = "link";
    private static final String TAG_PUB_DATE   = "pubDate";

    private static final String INPUT_ENCODING = "windows-1251";

    // Не используем XML namespace'ы
    private static final String ns = null;
    //endregion

    /** Разбирает RSS канал, возвращает коллекцию объектов типа {@link RssNewsItem}.
     *
     * @param in RSS канал, в виде InputStream.
     * @return List из объектов {@link RssNewsItem}.
     */
    public static List<RssNewsItem> parse(InputStream in)
            throws XmlPullParserException, IOException, ParseException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, INPUT_ENCODING);
            parser.nextTag(); // rss
            parser.nextTag(); // channel
            return readChannel(parser);
        } finally {
            in.close();
        }
    }

    /**
     * Декодирует channel прикреплённый к XmlPullParser.
     *
     * @param parser Входящая XMl
     * @return List из объектов {@link RssNewsItem}.
     */
    private static List<RssNewsItem> readChannel(XmlPullParser parser)
            throws XmlPullParserException, IOException, ParseException {
        List<RssNewsItem> entries = new ArrayList<RssNewsItem>();

        // Search for <channel> tag. https://cyber.harvard.edu/rss/rss.html
        //
        // Example:
        // <rss version="2.0">
        //  <channel>
        //   ...
        //  </channel>
        // </rss>

        parser.require(XmlPullParser.START_TAG, ns, TAG_CHANNEL);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();

            // Starts by looking for the <item> tag. This tag repeats inside of <channel> for each
            // article in the feed.
            //
            // Example (from http://alfabank.ru/_/rss/_rss.html):
            // <item>
            //    <title>
            //       Альфа Private лучший банковский бренд по мнению клиентов private banking - 2017 по версии Frank Research Group
            //    </title>
            //    <link>
            //       https://alfabank.ru/press/news/2017/6/20/39530.html?from=alfa_rss_news
            //    </link>
            //    <description>
            //       ...
            //    </description>
            //    <pubDate>
            //       Tue, 20 Jun 2017 14:42:00 GMT
            //    </pubDate>
            //    <guid>
            //       https://alfabank.ru/press/news/2017/6/20/39530.html
            //    </guid>
            // </item>
            if (name.equals(TAG_ITEM)) {
                entries.add(readItem(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }

    /**
     * Разбирает содержимое item'а. Если оно содержит теги title, link, guid или pub_date, передаёт
     * их для прочтения в метод readBasicTag(). В противном случае пропускает неизвестные теги.
     */
    private static RssNewsItem readItem(XmlPullParser parser)
            throws XmlPullParserException, IOException, ParseException {

        parser.require(XmlPullParser.START_TAG, ns, TAG_ITEM);
        String guid = null;
        String title = null;
        String link = null;
        long pub_date = 0;

        SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("MSK"));

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();

            switch (name) {
                case TAG_GUID:
                    // Example: <guid>https://alfabank.ru/press/news/2017/6/20/39530.html</guid>
                    guid = readBasicTag(parser, TAG_GUID);
                    break;
                case TAG_TITLE:
                    // Example: <title>Article title</title>
                    title = readBasicTag(parser, TAG_TITLE);
                    break;
                case TAG_LINK:
                    // Example: <link>https://alfabank.ru/press/news/2017/6/20/39530.html?from=alfa_rss_news</link>
                    link = readBasicTag(parser, TAG_LINK);
                    break;
                case TAG_PUB_DATE:
                    // Example: <pubDate>Tue, 20 Jun 2017 14:42:00 GMT</pubDate>
                    pub_date = AppDateUtils.getTimeInMillisFromGMT(
                            readBasicTag(parser, TAG_PUB_DATE), format);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }
        return new RssNewsItem(guid, title, link, pub_date);
    }

    /**
     * Читает содержимое простого XML тега, который гарантированно не имеет вложенных елементов
     *
     * @param parser Текущий объект для разбора
     * @param tag имя XML тега подвергаемого разбору
     * @return Строку являющуюся телом разбираемого тега
     */
    private static String readBasicTag(XmlPullParser parser, String tag)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String result = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return result;
    }

    /**
     * Для извлекает текстовое содержимое тегов.
     */
    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    /**
     * Пропускает теги которые не интересны парсеру.
     */
    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}