package com.samuelunknown.alfa_news.utilities;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class ToolBarUtils {

    /**
     * Устанавливает Toolbar в качестве ActionBar
     * @param activity активити
     * @param toolbarId id из ресурсов
     * @return установленный Toolbar
     */
    public static Toolbar setToolbarAsActionBar(AppCompatActivity activity, @IdRes int toolbarId) {
        Toolbar toolbar = (Toolbar) activity.findViewById(toolbarId);
        activity.setSupportActionBar(toolbar);

        return toolbar;
    }

    public static void setDisplayHomeAsUpEnabled(AppCompatActivity activity,
                                                 boolean displayHomeAsUpEnabled) {
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(displayHomeAsUpEnabled);
        }
    }

    public static void setDisplayShowHomeEnabled(AppCompatActivity activity,
                                                 boolean displayShowHomeEnabled) {
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(displayShowHomeEnabled);
        }
    }

    public static void setHomeAsUpIndicator(AppCompatActivity activity, @Nullable Drawable indicator) {
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(indicator);
        }
    }

    public static void setHomeAsUpIndicator(AppCompatActivity activity, @DrawableRes int resId) {
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(resId);
        }
    }
}
