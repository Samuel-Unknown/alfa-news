package com.samuelunknown.alfa_news.utilities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.util.TypedValue;

/**
 * Класс для работы с ресурсами приложения.
 */
public class ResourcesUtils {

    private static final String TAG = ResourcesUtils.class.getSimpleName();

    /**
     * Возвращает цвет заданный атрибутом из текущей темы приложения
     * @param context контекст
     * @param colorAttrResId атрибут задающий некоторый цвет и который определён темой приложения,
     * например R.attr.colorIconCons
     * @return цвет заданный атрибутом из текущей темы приложения
     */
    @ColorInt
    public static int getColorFromCurrentTheme(@NonNull Context context, @AttrRes int colorAttrResId) {

        @ColorInt int color = Color.BLACK;

        //region Способ #1 (используемый)
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(colorAttrResId, typedValue, true))
            color = typedValue.data;
        //endregion

        //region Способ #2 (неиспользуемы, но тоже рабочий)
//        int[] attrs = {colorAttrResId};
//        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs);
//
//        color = ta.getColor(0, Color.BLACK);
//        ta.recycle();
        //endregion

//        Log.i(TAG, Integer.toHexString(color));
        return color;
    }

    /**
     * Устанавливает цвет для GradientDrawable
     * @param gradientDrawable GradientDrawable для которого устанавливается цвет
     * @param color цвет который будет установлен
     */
    public static void setColorToGradientDrawable(@NonNull GradientDrawable gradientDrawable, @ColorInt int color) {
        GradientDrawable sd = (GradientDrawable) gradientDrawable.mutate();

        sd.setColor(color);
        sd.invalidateSelf();
    }
}
