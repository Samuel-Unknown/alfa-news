package com.samuelunknown.alfa_news.utilities;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.samuelunknown.alfa_news.news.RssNewsItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class NetworkUtils {

    private static final String TAG = NetworkUtils.class.getSimpleName();
    private static final OkHttpClient client = new OkHttpClient();

    public interface RssListener {
        void onReceiveRssData(InputStream data);
        void onFailureReceiveRssData(Exception e);
    }

    @Nullable
    public static URL buildURL(String uriString) {
        Uri uri = Uri.parse(uriString).buildUpon().build();
        URL url;

        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }

        return url;
    }

    @NonNull
    public static InputStream getRssData(@NonNull URL url)
            throws Exception {

        Response response = NetworkUtils.callRssRequest(url);

        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }

        ResponseBody responseBody = response.body();
        if (responseBody == null) {
            throw new NullPointerException("ResponseBody is null");
        }

        return responseBody.byteStream();
    }

    private static Response callRssRequest(@NonNull URL url)
            throws Exception {

        Request request = new Request.Builder()
                .url(url)
                .build();

        return client.newCall(request).execute();
    }

    public static void getRssDataAsync(@NonNull URL url, @NonNull final RssListener rssListener)
            throws Exception {

        NetworkUtils.callRssRequestAsync(url, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                rssListener.onFailureReceiveRssData(e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                if (!response.isSuccessful()) {
                    IOException e = new IOException("Unexpected code " + response);
                    rssListener.onFailureReceiveRssData(e);
                    return;
                }

                ResponseBody responseBody = response.body();
                if (responseBody == null) {
                    NullPointerException e = new NullPointerException("ResponseBody is null");
                    rssListener.onFailureReceiveRssData(e);
                    return;
                }

                rssListener.onReceiveRssData(responseBody.byteStream());
            }
        });
    }

    private static void callRssRequestAsync(@NonNull URL url, @NonNull Callback callback)
            throws Exception {

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(callback);
    }

    //region Только для отладки
    public static void printInputStreamLikeSimpleText(InputStream data) {
        try {
            //region Вывод сплошным текстом
            String dataString = "";
            int BUFFER_SIZE = 8192;

            BufferedReader br = new BufferedReader(new InputStreamReader(data, "windows-1251"), BUFFER_SIZE);
            String str;
            while ((str = br.readLine()) != null) {
                dataString += str;
            }
            System.out.println(dataString);
            //endregion
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printInputStreamLikeRssNewsItems(@NonNull Context context, InputStream data) {
        try {
            //region Вывод списком элементов RssNewsItem
            List<RssNewsItem> list = RssParserUtils.parse(data);

            for (RssNewsItem item : list) {
                Log.i(TAG, item.getTitle());
                Log.i(TAG, item.getLink());
                Log.i(TAG, AppDateUtils.getReadableDateString(context, item.getPubDate()));
                Log.i(TAG, item.getGuid());
            }
            //endregion
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //endregion
}
