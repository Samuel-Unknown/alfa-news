package com.samuelunknown.alfa_news.utilities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.samuelunknown.alfa_news.R;

public class CommunicationUtils {
    public static void openSendMailToDeveloper(Context context) {
        String email = context.getString(R.string.developer_email);
        String subject = context.getString(R.string.email_subject);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.fromParts("mailto", email, null));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, R.string.email_error, Toast.LENGTH_LONG).show();
        }
    }

    public static void shareLink(Context context, String link) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");

        intent.putExtra(Intent.EXTRA_TEXT,
                context.getString(R.string.share_link_text) + " " + link);

        Intent chooserIntent = Intent.createChooser(intent, context.getString(R.string.share_link_chooser_text));

        if (chooserIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(chooserIntent);
        } else {
            Toast.makeText(context, R.string.share_link_error, Toast.LENGTH_LONG).show();
        }
    }
}
