package com.samuelunknown.alfa_news.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.samuelunknown.alfa_news.R;
import com.samuelunknown.alfa_news.news.DbNewsItem;
import com.samuelunknown.alfa_news.news.NewsPresenter;
import com.samuelunknown.alfa_news.utilities.AppDateUtils;
import com.samuelunknown.alfa_news.utilities.ResourcesUtils;

import java.util.ArrayList;
import java.util.List;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsViewHolder> {

    public interface NewsListAdapterListener {
        void onNewsClick(int position);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = NewsListAdapter.class.getSimpleName();

    //region Variables
    private final NewsListAdapterListener mNewsListAdapterListener;

    private Context mContext;

    /**
     * Все новости полученные из БД
     */
    private List<DbNewsItem> mAllItems;
    //endregion

    //region Adapter
    public NewsListAdapter(Context context, NewsListAdapterListener listener) {
        mContext = context;
        mNewsListAdapterListener = listener;
        mAllItems = new ArrayList<>();
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext); // можно и так -> LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.news_list_item, parent, false);

        NewsViewHolder newsViewHolder = new NewsViewHolder(view);

        setupClickEvents(newsViewHolder);

        return newsViewHolder;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        DbNewsItem item = mAllItems.get(position);

        final long    id        = item.getId();
        final String  guid      = item.getGuid();
        final String  title     = item.getTitle();
        final String  link      = item.getLink();
        final long    putDate   = item.getPubDate();
        final boolean favorite  = item.getFavoriteBoolean();
        final long    timeStamp = item.getTimeStamp();

        holder.newsTitleTextView.setText(title);
        holder.newsIconTextView.setText(String.valueOf(title.toUpperCase().charAt(0)));
        holder.putDateTextView.setText(AppDateUtils.getReadableDateString(mContext, putDate));

        if (favorite) {
            setIconTextViewColor(holder, ResourcesUtils.getColorFromCurrentTheme(mContext,
                    R.attr.colorNewsIconTextBackgroundFavorite));
        } else {
            setIconTextViewColor(holder, ResourcesUtils.getColorFromCurrentTheme(mContext,
                    R.attr.colorNewsIconTextBackgroundNotFavorite));
        }
    }

    @Override
    public int getItemCount() {
        return mAllItems.size();
    }
    //endregion

    private void setIconTextViewColor(@NonNull NewsViewHolder holder, @ColorInt int color) {
        ResourcesUtils.setColorToGradientDrawable(
                (GradientDrawable) holder.newsIconTextView.getBackground(), color);
    }

    private void setupClickEvents(@NonNull final NewsViewHolder holder) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int adapterPosition = holder.getAdapterPosition();
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    mNewsListAdapterListener.onNewsClick(adapterPosition);
                }
            }
        });
    }

    public void swapCursor(@Nullable Cursor cursor) {
        mAllItems.clear();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                long   id        = cursor.getLong(NewsPresenter.INDEX_NEWS_ID);
                String guid      = cursor.getString(NewsPresenter.INDEX_NEWS_GUID);
                String title     = cursor.getString(NewsPresenter.INDEX_NEWS_TITLE);
                String link      = cursor.getString(NewsPresenter.INDEX_NEWS_LINK);
                long   putDate   = cursor.getLong(NewsPresenter.INDEX_NEWS_PUT_DATE);
                int    favorite  = cursor.getInt(NewsPresenter.INDEX_NEWS_FAVORITE);
                long   timeStamp = cursor.getLong(NewsPresenter.INDEX_NEWS_TIMESTAMP);

                mAllItems.add(new DbNewsItem(id, guid, title, link, putDate, favorite, timeStamp));
            }

            cursor.close();
        }

        notifyDataSetChanged();
    }

    public void swapDbNewsItem(int position, @NonNull DbNewsItem item) {
        mAllItems.set(position, item);
        notifyItemChanged(position);
    }

    public DbNewsItem getDbNewsItem(int position) {
        return mAllItems.get(position);
    }

    public List<DbNewsItem> getAllDbNewsItems() {
        List<DbNewsItem> items = new ArrayList<>();
        items.addAll(mAllItems);

        return items;
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        TextView newsIconTextView;
        TextView newsTitleTextView;
        TextView putDateTextView;

        NewsViewHolder(View itemView) {
            super(itemView);

            newsIconTextView = (TextView) itemView.findViewById(R.id.news_list_item_icon_text_view);
            newsTitleTextView = (TextView) itemView.findViewById(R.id.news_list_item_title_text_view);
            putDateTextView = (TextView) itemView.findViewById(R.id.news_list_item_put_date_text_view);
        }
    }
}
